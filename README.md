# Golang+PHP+MySQL实现的DHT网络爬虫整站程序

作者博客：<http://www.lovekk.org>

# 程序说明

本程序使用Golang+PHP开发，Golang负责数据采集，PHP负责前台显示，数据库使用MySQL。

# 安装说明

* 安装Golang
* 安装Git
* 安装MySQL
* 安装PHP

## 获取所使用的第三方库

```bash
go get github.com/astaxie/beego
go get github.com/zeebo/bencode
go get github.com/go-sql-driver/mysql
go get github.com/wangbin/jiebago
```

## 导入数据库

1. 新建一个数据库并分配相应权限
2. 使用任意工具将 import.sql 中的内容导入到数据库中

## 配置conf/app.conf

```
RunMode = dev # 无需理会
showmsg = true # 是否输出采集信息
dbhost = localhost # MySQL连接地址
dbport = 3306 # MySQL连接端口
dbname = SCDht # MySQL数据库名
dbuser = root # MySQL连接用户名
dbpass = 123456 # MySQL连接密码
```

## 配置conf/filter.txt

此文件内为过滤内容列表，以 | 分割，在采集时自动设置包含此关键字的内容为不显示。

## 开启采集程序

进入源码目录，运行：

```bash
./run
```

# 授权方式

本程序遵循MIT授权

# 程序反馈

<http://www.lovekk.org>
