<?php
namespace Home\Controller;
use Think\Controller;
class EmptyController extends Controller{
    public function index(){
        show404();
    }
    public function _empty(){
        show404();
    }
}