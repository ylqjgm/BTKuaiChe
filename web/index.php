<?php
// 检测PHP环境
if(version_compare(PHP_VERSION,'5.3.0','<'))  die('require PHP > 5.3.0 !');

// 开启调试模式 建议开发阶段开启 部署阶段注释或者设为false
define('APP_DEBUG',true);

define('DICT_PATH', './static/dict/');

// 生成安全文件
define('DIR_SECURE_FILENAME', 'index.html');

// 运行时目录
define('RUNTIME_PATH', './data/');

// 定义应用目录
define('APP_PATH','./');

// 引入ThinkPHP入口文件
require './Core/Core.php';

// 亲^_^ 后面不需要任何代码了 就是如此简单