<?php
namespace Home\Model;
use Think\Model\RelationModel;

class HashModel extends RelationModel{
    protected $_link = array(
        'File' => array(
            'mapping_type'  => self::HAS_MANY,
            'class_name' => 'File',
            'foreign_key' => 'torrent_id',
            'mapping_name' => 'file',
        ),
    );
}