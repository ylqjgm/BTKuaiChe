<?php
return array(
    'SHOW_PAGE_TRACE' => true, // 显示trace
    'DB_TYPE' => 'mysql', // 数据库类型
    'DB_HOST' => 'localhost', // 数据库地址
    'DB_NAME' => 'btkuaiche', // 数据库名
    'DB_USER' => 'root', // 用户名
    'DB_PWD' => 'yllm.net', // 密码
    'DB_PORT' => 3306, // 端口
    'DB_PREFIX' => 'bt_', // 表前缀
    'DB_CHARSET' => 'utf8', // 字符集
    'DEFAULT_CHARSET' => 'utf-8', // 默认字符集
    'URL_CASE_INSENSITIVE' => true,
    'URL_MODEL' => 4, // URL访问模式
    'URL_ROUTER_ON' => true, // 开启路由
    'URL_ROUTE_RULES' => array( // 路由规则
        'putlog' => 'Index/putlog', // 记录搜索信息
        'new' => 'Index/news', // 最新收录
        'tags' => 'Index/tags', // 标签列表
        'list/:key/:p' => 'Index/search', // 查询列表页分页规则
        'list/:key' => 'Index/search', // 查询列表页规则
        ':hash' => 'Index/view', // 显示种子信息页面
    ),
    'DATA_CACHE_TIME' => 3600, // 缓存时间
    'DATA_CACHE_PREFIX' => 'btkuaiche_', // 缓存前缀
    'DATA_CACHE_TYPE' => 'File', // 缓存类型
    'DB_SQL_BUILD_CACHE' => true, // 开启查询缓存
    'DB_SQL_BUILD_LENGTH' => 20, // 查询缓存列队长度
    'HTML_CACHE_ON' => true, // 开启静态缓存
    'HTML_CACHE_TIME' => 0, // 静态缓存时间
    'HTML_FILE_SUFFIX' => '.html', // 静态缓存后缀
    'HTML_CACHE_RULES' => array( // 静态缓存规则
        'index:view' => array('{hash}', 0),
    ),
    'LANG_SWITCH_ON' => true, // 开启多语言
    'LANG_AUTO_DETECT' => true, // 自动侦测语言
    'LANG_LIST' => 'en-us,ja-jp,ko-kr,zh-cn,zh-tw', // 允许切换的语言列表
    'VAR_LANGUAGE' => 'l', // 默认语言切换变量
);
