<?php
namespace Home\Controller;
use Think\Controller;
class IndexController extends Controller {
    /**
     * 网站首页
     * @return void
     */
    public function index(){
        header('Content-type: text/html; charset=utf-8');
        // 获取hotsearch缓存
        $hotsearch = S('hotsearch');

        // 检测缓存是否存在, 不存在则查询
        if(empty($hotsearch)){
            // 初始化bt_search表
            $search = M('Search');
            // 获取最热搜索
            $hotsearch = $search->cache('hotsearch')->order('logtime desc')->limit(5)->select();
        }

        // 获取allcount缓存
        $allcount = S('allcount');

        // 检测缓存
        if(empty($allcount)){
            // 获取总数
            $allcount = M('Hash')->cache('allcount')->count();
        }

        // 设置模板数据
        $data['hotsearch'] = $hotsearch;
        $data['allcount'] = $allcount;
        $data['lang'] = cookie('think_language') ? cookie('think_language') : 'zh-CN';

        // 传入模板
        $this->assign($data);
        // 显示模板
        $this->display();
    }

    /**
     * 数据搜索
     * @return void
     */
    public function search(){
        header('Content-type: text/html; charset=utf-8');
        // 获取搜索关键字
        $key = I('get.key', '');
        // 检测是否为空
        if(empty($key)){
            // 显示404
            show404();
        }
        // 进行url解码
        $key = urldecode($key);

        // 获取排序方式
        $sort = I('get.sort', '');
        // 检测是否为空
        if(empty($sort)){
            $sort = 'create_date';
        }

        // 获取当前分页
        $p = I('get.p', 1, 'int');

        // 获取数据缓存
        $list = S('list_'.$key.'_'.$p);
        $count = S('count_'.$key);
        $page = S('page_'.$key);
        $news = S('news');

        // 检测缓存是否存在
        if(empty($list) || empty($count) || empty($page)){
            // 初始化bt_hash
            $hash = D('Hash');

            // 设置查询条件
            $conditon['name'] = array('like', '%'.$key.'%');

            // 获取数据总量
            $count = $hash->cache('count_'.$key)->where($conditon)->count();

            // 初始化分页类
            $Page = new \Think\Page($count, 15);
            // 设置链接地址
            $Page->setConfig('link', '/list/'.$key.'/page.html');
            // 分页显示输出
            $page = $Page->show();

            // 分页数据查询
            $list = $hash->cache('list_'.$key.'_'.$p)->relation(true)->where($conditon)->order($sort.' desc')->limit($Page->firstRow.','.$Page->listRows)->select();

            // 获取最新入库
            $news = $hash->cache('news')->limit(10)->order('create_at desc')->select();
        }

        // 获取缓存数据
        $relevant = S('relevant_'.$key);
        $last = S('last');

        // 初始化Search
        $search = M('Search');
        // 获取随机推荐数据
        $random = $search->limit(10)->order('rand()')->select();

        // 检测缓存是否存在
        if(empty($relevant) || empty($last)){
            // 获取相关搜索
            $relevant = $search->cache('relevant_'.$key)->where(array('keyword'=>array('like','%'.$key.'%')))->order('keyword asc')->limit(10)->select();

            // 获取最后搜索
            $last = $search->cache('last')->order('logtime desc')->limit(10)->select();
        }

        // 获取hotsearch缓存
        $hotsearch = S('hotsearch');

        // 检测缓存是否存在, 不存在则查询
        if(empty($hotsearch)){
            // 初始化bt_search表
            $search = M('Search');
            // 获取最热搜索
            $hotsearch = $search->cache('hotsearch')->order('logtime desc')->limit(5)->select();
        }

        // 设置模板数据
        $data['list'] = $list;
        $data['page'] = $page;
        $data['keyword'] = $key;
        $data['sort'] = $sort;
        $data['count'] = $count;
        $data['relevant'] = $relevant;
        $data['last'] = $last;
        $data['random'] = $random;
        $data['news'] = $news;
        $data['hotsearch'] = $hotsearch;
        $data['lang'] = cookie('think_language') ? cookie('think_language') : 'zh-CN';

        // 传入模板
        $this->assign($data);
        // 显示模板
        $this->display();
    }

    /**
     * 显示种子信息页面
     * @return void
     */
    public function view(){
        header('Content-type: text/html; charset=utf-8');
        // 获取hash数据
        $hash = I('get.hash', '');
        // 检测hash是否正确
        if(empty($hash) || strlen($hash)<40)
            show404();

        // 获取缓存数据
        $bt = S('hash_'.$hash);
        // 检测缓存是否存在
        if(empty($bt)){
            // 初始化Hash
            $h = D('Hash');
            // 获取种子信息
            $bt = $h->cache('hash_'.$hash)->relation(true)->where(array('info_hash'=>$hash))->find();
        }

        // 获取hotsearch缓存
        $hotsearch = S('hotsearch');
        // 检测缓存是否存在, 不存在则查询
        if(empty($hotsearch)){
            // 初始化bt_search表
            $search = M('Search');
            // 获取最热搜索
            $hotsearch = $search->cache('hotsearch')->order('logtime desc')->limit(5)->select();
        }

        // 模板赋值
        $data['name'] = $bt['name'];
        $data['create_date'] = $bt['create_date'];
        $data['create_at'] = $bt['create_at'];
        $data['file_len'] = $bt['file_len'];
        $data['hot'] = $bt['hot'];
        $data['file_count'] = $bt['file_count'];
        $data['info_hash'] = $bt['info_hash'];
        $data['files'] = $bt['file'];
        $data['hotsearch'] = $hotsearch;
        $data['lang'] = cookie('think_language') ? cookie('think_language') : 'zh-CN';

        // 传入模板
        $this->assign($data);
        // 显示模板
        $this->display();
    }

    /**
     * 最新收录
     * @return void
     */
    public function news(){
        // 获取hotsearch缓存
        $hotsearch = S('hotsearch');
        // 检测缓存是否存在, 不存在则查询
        if(empty($hotsearch)){
            // 初始化bt_search表
            $search = M('Search');
            // 获取最热搜索
            $hotsearch = $search->cache('hotsearch')->order('logtime desc')->limit(5)->select();
        }

        // 获取数据缓存
        $list = S('new');

        // 检测缓存是否存在
        if(empty($list)){
            // 初始化bt_hash
            $hash = D('Hash');

            // 分页数据查询
            $list = $hash->cache('new')->relation(true)->order('create_at desc')->limit(15)->select();
        }

        // 获取缓存数据
        $last = S('last');

        // 检测缓存是否存在
        if(empty($last)){
            // 初始化Search
            $search = M('Search');

            // 获取最后搜索
            $last = $search->cache('last')->order('logtime desc')->limit(10)->select();
        }

        // 模板赋值
        $data['hotsearch'] = $hotsearch;
        $data['list'] = $list;
        $data['last'] = $last;
        $data['lang'] = cookie('think_language') ? cookie('think_language') : 'zh-CN';

        // 传入模板
        $this->assign($data);
        // 显示模板
        $this->display('new');
    }

    /**
     * 搜索标签列表
     * @return void
     */
    public function tags(){
        // 获取hotsearch缓存
        $hotsearch = S('hotsearch');
        // 检测缓存是否存在, 不存在则查询
        if(empty($hotsearch)){
            // 初始化bt_search表
            $search = M('Search');
            // 获取最热搜索
            $hotsearch = $search->cache('hotsearch')->order('logtime desc')->limit(5)->select();
        }

        // 获取缓存数据
        $tags = S('tags');
        // 检测缓存是否存在
        if(empty($tags)){
            // 初始化Tags
            $t = M('Tags');
            // 获取标签列表
            $tags = $t->cache('tags')->select();
        }

        // 模板赋值
        $data['hotsearch'] = $hotsearch;
        $data['tags'] = $tags;
        $data['lang'] = cookie('think_language') ? cookie('think_language') : 'zh-CN';

        // 传入模板
        $this->assign($data);
        // 显示模板
        $this->display();
    }

    /**
     * 记录搜索信息
     * @return void
     */
    public function putlog(){
        header('Content-type: text/html; charset=utf-8');
        // 获取关键词
        $key = I('get.k', '');
        // 检测关键词是否传入
        if(empty($key))
            return '';

        // 初始化Search
        $search = M('Search');
        // 获取该关键词信息
        $data = $search->where(array('keyword'=>$key))->find();

        // 检测关键词是否已经存在
        if(is_array($data)){
            // 搜索次数自增1
            $data['nums'] += 1;
            // 最后搜索时间为此时
            $data['logtime'] = time();
            // 修改数据
            $search->save($data);
        }else{
            // 设置关键词
            $data['keyword'] = $key;
            // 设置搜索时间
            $data['logtime'] = time();
            // 设置搜索次数
            $data['nums'] = 1;
            // 新增数据
            $search->add($data);
        }
    }
}
