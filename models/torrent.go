package models

import (
	"errors"
	"time"

	"github.com/astaxie/beego/orm"
)

type Torrent struct {
	Id       int64  `json:"id"`
	InfoHash string `orm:"unique"  json:"infoHash"`
	Name     string `orm:"type(text)" json:"name"`
	//	FileType       string    `orm:"size(30)"`
	LastDownloadAt int64   `orm:"null;json:"lastDownDate"`
	LastDownloadIp string  `orm:"null;size(50)"`
	Hot            uint64  `json:"hot"` // 下载热度
	Views          int     `json:"views"`
	FileLen        uint64  `json:"fileLength"`   // 文件大小
	FileCount      int     `json:"fileCount"`    // 文件数量
	Enable         bool    `json:"enable"`       // 审核标志
	FileList       []*File `orm:"reverse(many)"` // multiple files.
	CreateDate     int64   `json:"createDate"`
	CreateAt       int64   `json:"-"`
}

func (t *Torrent) TableName() string {
	return "bt_hash"
}

type File struct {
	Id       int64    `json:"id"`
	Torrent  *Torrent `orm:"rel(fk);on_delete(do_nothing)"`
	Name     string   `orm:"type(text)"`
	Length   uint64
	CreateAt int64 `json:"-"`
}

func (f *File) TableName() string {
	return "bt_file"
}

// isHave
func (t *Torrent) IsExists() bool {
	o := orm.NewOrm()
	ip := t.LastDownloadIp
	downloadTime := t.LastDownloadAt
	if err := o.QueryTable("bt_hash").Filter("info_hash", t.InfoHash).One(t); err != nil {
		// 不存在
		return false
	}

	// 存在,更新最后下载IP 和时间和热度.
	go func(t *Torrent, o orm.Ormer) {
		if t.LastDownloadIp != ip {
			t.LastDownloadIp = ip
			t.LastDownloadAt = downloadTime
			t.Hot += 1
			o.Update(t, "last_download_ip", "last_download_at", "hot")
		}
	}(t, o)
	return true
}

// new torrent
func (t *Torrent) Create() error {
	o := orm.NewOrm()
	if t.IsExists() {
		return errors.New("Torrent already in database;")
	}
	// start transaction
	if err := o.Begin(); err != nil {
		return err
	}
	// get file length and file count.
	if t.FileList != nil {
		t.FileCount = len(t.FileList)
		t.FileLen = 0
		for _, file := range t.FileList {
			file.Torrent = t
			file.CreateAt = time.Now().Unix()
			t.FileLen += file.Length
		}
	} else {
		t.FileCount = 1
	}
	// insert torrent.
	t.Hot = 1
	t.CreateAt = time.Now().Unix()
	if _, err := o.Insert(t); err != nil {
		o.Rollback()
		return err
	}
	// insert torrent file.
	if t.FileList != nil {
		if _, err := o.InsertMulti(100, t.FileList); err != nil {
			o.Rollback()
			return err
		}
	}
	// commit transaction.
	if err := o.Commit(); err != nil {
		return err
	}
	return nil
}
