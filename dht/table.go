package dht

import (
	"errors"
)

// dht routing
type Routing struct {
	selfNode *Node
	buckets  []*Bucket
}

// new routing
func NewRouting(node *Node) *Routing {
	routing := &Routing{}

	routing.selfNode = node
	routing.buckets = make([]*Bucket, 2)
	routing.buckets[0] = NewBucket()
	routing.buckets[1] = NewBucket()

	return routing
}

// Insert Node
func (r *Routing) InsertNode(node *ContactInfo) error {
	// 如果新插入的node的ID长度不为20位,终止.
	if len(node.Id) != 20 {
		return errors.New("Add node into the routing table failed, the node id lenght invalid.")
	}

	// 如果待插入的是自己本身,则忽略.
	if node.Id.String() == r.selfNode.info.Id.String() {
		return nil
	}

	// 现将1号填满
	if r.buckets[1].Len() < 8 {
		r.buckets[1].Add(node)
	} else {
		r.buckets[0].Add(node)
	}

	return nil
}
