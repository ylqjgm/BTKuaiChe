DROP TABLE IF EXISTS `bt_hash`;
CREATE TABLE IF NOT EXISTS `bt_hash` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `info_hash` char(40) NOT NULL DEFAULT '',
  `name` varchar(255) NOT NULL DEFAULT '',
  `last_download_at` int(10) unsigned DEFAULT NULL,
  `last_download_ip` varchar(15) DEFAULT NULL,
  `hot` int(10) unsigned NOT NULL DEFAULT 0,
  `views` int(10) unsigned NOT NULL DEFAULT 0,
  `file_len` bigint(20) unsigned NOT NULL DEFAULT 0,
  `file_count` int(10) unsigned NOT NULL DEFAULT 0,
  `enable` tinyint(1) NOT NULL DEFAULT 0,
  `create_date` int(10) unsigned NOT NULL,
  `create_at` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `infohash` (`info_hash`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

DROP TABLE IF EXISTS `bt_file`;
CREATE TABLE IF NOT EXISTS `bt_file` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `torrent_id` bigint(20) NOT NULL,
  `name` varchar(255) NOT NULL,
  `length` bigint(20) unsigned NOT NULL DEFAULT 0,
  `create_at` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

DROP TABLE IF EXISTS `bt_search`;
CREATE TABLE IF NOT EXISTS `bt_search` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `keyword` varchar(100) NOT NULL,
    `logtime` int(10) unsigned NOT NULL,
    `nums` int(11) NOT NULL DEFAULT 1 ,
    PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

DROP TABLE IF EXISTS `bt_tags`;
CREATE TABLE IF NOT EXISTS `bt_tags` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT ,
  `tag` varchar(100) NOT NULL ,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
