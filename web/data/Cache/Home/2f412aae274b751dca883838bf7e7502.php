<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html lang="<?php echo ($lang); ?>">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo (L("home_title")); ?></title>
    <meta name="keywords" content="<?php echo (L("home_keywords")); ?>">
    <meta name="description" content="<?php echo (L("home_description")); ?>">
    <link rel="icon" type="image/png" href="/static/img/favicon.png">
    <link rel="stylesheet" href="//cdn.bootcss.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <link rel="stylesheet" href="/static/css/style.css">
    <!--[if lt IE 9]>
    <script src="//cdn.bootcss.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="//cdn.bootcss.com/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body class="home">
<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/"><?php echo (L("site_name")); ?></a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav navbar-right">
                <li><a href="/new.html"><i class="glyphicon glyphicon-fire"></i> <?php echo (L("new_menu")); ?></a></li>
                <li><a href="/tags.html"><i class="glyphicon glyphicon-tags"></i> <?php echo (L("tags_menu")); ?></a></li>
                <li class="dropdown">
                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="glyphicon glyphicon-globe"></i> Language <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a onclick="javascript:changeLanguage('en-US');">English</a></li>
                        <li><a onclick="javascript:changeLanguage('zh-TW');">繁體中文</a></li>
                        <li><a onclick="javascript:changeLanguage('zh-CN');">简体中文</a></li>
                        <li><a onclick="javascript:changeLanguage('ja-JP');">日本語</a></li>
                        <li><a onclick="javascript:changeLanguage('ko-KR');">한국어</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</nav>

<div class="container">
    <div class="row">
        <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2"></div>
        <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
            <div class="search">
                <div class="home-logo center-block"></div>
                <div class="input-group input-group-lg" style="margin-top:10px">
                    <input type="text" class="form-control" id="key" placeholder="<?php echo L('search_key',array('allcount'=>$allcount));?>">
                    <span class="input-group-btn"><button class="btn btn-info" type="submit" id="search"><i class="glyphicon glyphicon-search"></i></button></span>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="hot text-center  hidden-sm hidden-xs">
                <ul class="list-inline">
                    <?php if(is_array($hotsearch)): $i = 0; $__LIST__ = $hotsearch;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><li><a href="/list/<?php echo (urlencode($vo["keyword"])); ?>.html" title="<?php echo ($vo["keyword"]); ?>" class="label label-info"><?php echo ($vo["keyword"]); ?></a></li><?php endforeach; endif; else: echo "" ;endif; ?>
                </ul>
            </div>
        </div>
    </div>
</div>
<footer class="home-footer">
    <p>Copyright &copy;2015 <a href="/"><?php echo (L("site_name")); ?></a>. All Rights Reserved.</p>
</footer>
<script src="//cdn.bootcss.com/jquery/2.2.0/jquery.min.js"></script>
<script src="//cdn.bootcss.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
<script src="/static/js/common.js"></script>
<script>
$(function(){
    $('#key').focus();
});
</script>
</body>
</html>