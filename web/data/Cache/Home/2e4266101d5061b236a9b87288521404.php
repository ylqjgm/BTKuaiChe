<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html lang="<?php echo ($lang); ?>">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo L('list_title',array('keyword'=>$keyword));?></title>
    <meta name="keywords" content="<?php echo L('list_keywords',array('keyword'=>$keyword));?>">
    <meta name="description" content="<?php echo L('list_description',array('keyword'=>$keyword));?>">
    <link rel="icon" type="image/png" href="/static/img/favicon.png">
    <link rel="stylesheet" href="//cdn.bootcss.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <link rel="stylesheet" href="//cdn.bootcss.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="/static/css/style.css">
    <!--[if lt IE 9]>
    <script src="//cdn.bootcss.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="//cdn.bootcss.com/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<div class="header">
    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="col-md-1 col-lg-1"></div>
                <div class="col-md-2 col-lg-2 logo hidden-xs hidden-sm"><a href="/"><h1><?php echo (L("site_name")); ?></h1></a></div>
                <div class="col-xs-12 col-sm-12 col-md-7 col-lg-7">
                    <div class="input-group input-group-lg">
                        <input type="text" id="key" class="form-control" value="<?php echo ($keyword); ?>">
                        <span class="input-group-btn"><button class="btn btn-info" type="button" id="search"><i class="glyphicon glyphicon-search"></i></button></span>
                    </div>
                </div>
            </div>
        </div>
        <div class="row hots text-right hidden-xs hidden-sm">
            <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3"></div>
            <div class="col-xs-7 col-sm-7 col-md-7 col-lg-7">
                <span><?php if(is_array($hotsearch)): $i = 0; $__LIST__ = $hotsearch;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><a  href="/list/<?php echo (urlencode($vo["keyword"])); ?>.html" title="<?php echo ($vo["keyword"]); ?>"><?php echo ($vo["keyword"]); ?></a><?php endforeach; endif; else: echo "" ;endif; ?></span>
                <div class="fr">
                    <i class="fa fa-globe"></i> Language:
                    <select class="language" autocomplete="off" onchange="changeLanguage(this.value)">
                        <?php if(($lang) == "en-US"): ?><option value="en-US" selected="true">English</option><?php else: ?><option value="en-US">English</option><?php endif; ?>
                        <?php if(($lang) == "zh-TW"): ?><option value="zh-TW" selected="true">繁體中文</option><?php else: ?><option value="zh-TW">繁體中文</option><?php endif; ?>
                        <?php if(($lang) == "zh-CN"): ?><option value="zh-CN" selected="true">简体中文</option><?php else: ?><option value="zh-CN">简体中文</option><?php endif; ?>
                        <?php if(($lang) == "ja-JP"): ?><option value="ja-JP" selected="true">日本語</option><?php else: ?><option value="ja-JP">日本語</option><?php endif; ?>
                        <?php if(($lang) == "ko-KR"): ?><option value="ko-KR" selected="true">한국어</option><?php else: ?><option value="ko-KR">한국어</option><?php endif; ?>
                    </select>
                </div>
            </div>
        </div>
        <div class="row sort text-right">
            <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1"></div>
            <div class="col-xs-12 col-sm-12 col-md-10 col-lg-10">
                <ol class="list-inline">
                    <span class="hidden-xs"><?php echo L('list_text',array('keyword'=>$keyword,'count'=>$count));?></span>
                    <?php echo (L("sort_menu")); ?>
                    <?php if(($sort) == "create_date"): ?><li class="highlight"><?php echo (L("sort_create_date")); ?></li>
                    <?php else: ?>
                    <li><a href="?sort=create_date"><?php echo (L("sort_create_date")); ?></a></li><?php endif; ?>
                    <?php if(($sort) == "hot"): ?><li class="highlight"><?php echo (L("sort_hot")); ?></li>
                    <?php else: ?>
                    <li><a href="?sort=hot"><?php echo (L("sort_hot")); ?></a></li><?php endif; ?>
                    <?php if(($sort) == "file_len"): ?><li class="highlight"><?php echo (L("sort_file_len")); ?></li>
                    <?php else: ?>
                    <li><a href="?sort=file_len"><?php echo (L("sort_file_len")); ?></a></li><?php endif; ?>
                </ol>
            </div>
        </div>
    </div>
</div>

<div class="container-fluid">
    <div class="row">
        <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1"></div>
        <div class="col-xs-11 col-sm-11 col-md-7 col-lg-7">
            <ul class="media-list media-list-set">
                <?php if(is_array($list)): $i = 0; $__LIST__ = $list;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><li class="media">
                    <div class="media-body">
                        <h4><a class="title" href="/<?php echo ($vo["info_hash"]); ?>.html" title="<?php echo ($vo["name"]); ?>"><?php echo (highlight($vo["name"],$keyword)); ?></a></h4>
                        <div class="media-file">
                            <ul>
                                <?php if(empty($vo['file'])): ?><li><i class="fa <?php echo (file_type($vo["name"])); ?>"></i><?php echo ($vo["name"]); ?> <em><?php echo (size($vo["file_len"])); ?></em></li><?php endif; ?>
                                <?php if(is_array($vo['file'])): $i = 0; $__LIST__ = $vo['file'];if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$f): $mod = ($i % 2 );++$i; if(($i) <= "3"): ?><li><i class="fa <?php echo (file_type($f["name"])); ?>"></i><?php echo ($f["name"]); ?> <em><?php echo (size($f["length"])); ?></em></li><?php endif; ?>
                                <?php if(($i) == "4"): ?><li>...</li><?php endif; endforeach; endif; else: echo "" ;endif; ?>
                            </ul>
                        </div>
                        <div class="media-more">
                            <span><?php echo (L("list_files")); ?></span><label><?php echo ($vo["file_count"]); ?></label>
                            <span><?php echo (L("list_size")); ?></span><label><?php echo (size($vo["file_len"])); ?></label>
                            <span><?php echo (L("list_hot")); ?></span><label><?php echo ($vo["hot"]); ?></label>
                            <div class="media-down">
                                <a href="magnet:?xt=urn:btih:<?php echo ($vo["info_hash"]); ?>&dn=<?php echo (urlencode($vo["name"])); ?>" title="<?php echo (L("list_magnet_down")); ?> <?php echo ($vo["name"]); ?>"><i class="fa fa-magnet"></i> <?php echo (L("list_magnet")); ?></a>
                                <a href="<?php echo (thunder($vo["info_hash"])); ?>" title="<?php echo (L("list_thunder_down")); ?> <?php echo ($vo["name"]); ?>"><i class="fa fa-download"></i> <?php echo (L("list_thunder")); ?></a>
                            </div>
                        </div>
                    </div>
                </li><?php endforeach; endif; else: echo "" ;endif; ?>
            </ul>
            <?php echo ($page); ?>
        </div>
        <div class="col-md-3 col-lg-3 hidden-xs hidden-sm sidebar">
            <div class="widget">
                <h3><span class="badge">HOT</span>&nbsp;&nbsp;<?php echo (L("relevant")); ?></h3>
                <ul>
                    <?php if(is_array($relevant)): $i = 0; $__LIST__ = $relevant;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><li><a href="/list/<?php echo (urlencode($vo["keyword"])); ?>.html" title="<?php echo ($vo["keyword"]); ?>"><?php echo ($vo["keyword"]); ?></a><span><i class="fa fa-clock-o"></i> <?php echo (date_time($vo["logtime"])); ?></span></li><?php endforeach; endif; else: echo "" ;endif; ?>
                </ul>
            </div>
            <div class="widget">
                <h3><span class="badge">HOT</span>&nbsp;&nbsp;<?php echo (L("last_search")); ?></h3>
                <ul>
                    <?php if(is_array($last)): $i = 0; $__LIST__ = $last;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><li><a href="/list/<?php echo (urlencode($vo["keyword"])); ?>.html" title="<?php echo ($vo["keyword"]); ?>"><?php echo ($vo["keyword"]); ?></a><span><i class="fa fa-clock-o"></i> <?php echo (date_time($vo["logtime"])); ?></span></li><?php endforeach; endif; else: echo "" ;endif; ?>
                </ul>
            </div>
            <div class="widget">
                <h3><span class="badge">HOT</span>&nbsp;&nbsp;<?php echo (L("random")); ?></h3>
                <ul>
                    <?php if(is_array($random)): $i = 0; $__LIST__ = $random;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><li><a href="/list/<?php echo (urlencode($vo["keyword"])); ?>.html" title="<?php echo ($vo["keyword"]); ?>"><?php echo ($vo["keyword"]); ?></a><span><i class="fa fa-clock-o"></i> <?php echo (date_time($vo["logtime"])); ?></span></li><?php endforeach; endif; else: echo "" ;endif; ?>
                </ul>
            </div>
            <div class="widget">
                <h3><span class="badge">New</span>&nbsp;&nbsp;<?php echo (L("list_new")); ?></h3>
                <ul>
                    <?php if(is_array($news)): $i = 0; $__LIST__ = $news;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><li><a href="/<?php echo ($vo["info_hash"]); ?>.html" title="<?php echo ($vo["name"]); ?>" target="_blank"><?php echo ($vo["name"]); ?></a><span><i class="fa fa-clock-o"></i> <?php echo (date_time($vo["create_at"])); ?></span></li><?php endforeach; endif; else: echo "" ;endif; ?>
                </ul>
            </div>
        </div>
    </div>
</div>
<footer class="footer">
    <p>Copyright &copy;2015 <a href="/"><?php echo (L("site_name")); ?></a>. All Rights Reserved.</p>
</footer>
<div id="gotop"><i class="fa fa-arrow-circle-up"></i></div>
<script src="//cdn.bootcss.com/jquery/2.2.0/jquery.min.js"></script>
<script src="//cdn.bootcss.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
<script src="/static/js/common.js"></script>
</body>
</html>