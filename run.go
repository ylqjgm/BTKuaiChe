package main

import (
	"errors"
	"fmt"
	"strings"
	"time"

	"github.com/astaxie/beego"

	"gogs.vicp.co/ylqjgm/BTKuaiChe/dht"
	"gogs.vicp.co/ylqjgm/BTKuaiChe/filter"
	"gogs.vicp.co/ylqjgm/BTKuaiChe/models"
	"gogs.vicp.co/ylqjgm/BTKuaiChe/torrent"
)

// insert metainfo to database.
func Insert(infoHash string, ip string) error {
	t := &models.Torrent{
		LastDownloadIp: ip,
		LastDownloadAt: time.Now().Unix(),
		InfoHash:       strings.ToLower(infoHash),
	}

	if t.IsExists() {
		return errors.New("Torrent already in database;")
	}

	// get torrent file information.
	torrentFile := torrent.New(infoHash)
	m, err := torrentFile.GetTorrentMetaInfo()
	if err != nil {
		return err
	}

	if m.Info.Name8 != "" {
		t.Name = m.Info.Name8
	} else {
		t.Name = m.Info.Name
	}
	t.Enable = true
	if len(t.Name) < 2 || len([]byte(t.Name)) < 2 {
		return errors.New("Torrent info name length lt 2!")
	} else if filter.IsIllegalWords(t.Name) {
		// 非法资源
		t.Enable = false
	}

	t.FileLen = m.Info.Length
	t.CreateDate = m.CreateDate
	if t.CreateDate == 0 {
		t.CreateDate = time.Now().Unix()
	}

	// file list
	for _, v := range m.Info.Files {
		// 非法资源
		if filter.IsIllegalWords(v.Path[0]) {
			t.Enable = false
		}

		file := &models.File{
			Length: v.Length,
		}

		if v.Path8 != nil {
			file.Name = v.Path8[0]
		} else {
			file.Name = v.Path[0]
		}

		t.FileList = append(t.FileList, file)
	}

	return t.Create()
}

func excute(output chan string, port int) {
	myDHTNode := dht.NewDHTNode(output, port)
	myDHTNode.Run()
}

func main() {
	output := make(chan string)

	excute(output, 51972)

	showmsg, _ := beego.AppConfig.Bool("showmsg")

	for {
		select {
		case dat := <-output:
			s := strings.Split(dat, ":")

			infohash := s[0]
			ip := s[1]

			go func(info string, ip string) {
				if showmsg {
					if err := Insert(info, ip); err != nil {
						fmt.Printf("%s insert failed: %s \n", info, err.Error())
					} else {
						fmt.Printf("%s insert success! \n", info)
					}
				} else {
					Insert(info, ip)
				}
			}(infohash, ip)
		}
	}
}
