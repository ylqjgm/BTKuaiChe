<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html lang="<?php echo ($lang); ?>">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo (L("tags_title")); ?></title>
    <meta name="keywords" content="<?php echo (L("new_keywords")); ?>">
    <meta name="description" content="<?php echo (L("home_description")); ?>">
    <link rel="icon" type="image/png" href="/static/img/favicon.png">
    <link rel="stylesheet" href="//cdn.bootcss.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <link rel="stylesheet" href="//cdn.bootcss.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="/static/css/style.css">
    <!--[if lt IE 9]>
    <script src="//cdn.bootcss.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="//cdn.bootcss.com/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<div class="header">
    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="col-md-1 col-lg-1"></div>
                <div class="col-md-2 col-lg-2 logo hidden-xs hidden-sm"><a href="/"><h1><?php echo (L("site_name")); ?></h1></a></div>
                <div class="col-xs-12 col-sm-12 col-md-7 col-lg-7">
                    <div class="input-group input-group-lg">
                        <input type="text" id="key" class="form-control" placeholder="<?php echo (L("view_search")); ?>">
                        <span class="input-group-btn"><button class="btn btn-info" type="button" id="search"><i class="glyphicon glyphicon-search"></i></button></span>
                    </div>
                </div>
            </div>
        </div>
        <div class="row hots text-right hidden-xs hidden-sm">
            <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3"></div>
            <div class="col-xs-7 col-sm-7 col-md-7 col-lg-7">
                <span><?php if(is_array($hotsearch)): $i = 0; $__LIST__ = $hotsearch;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><a  href="/list/<?php echo (urlencode($vo["keyword"])); ?>.html" title="<?php echo ($vo["keyword"]); ?>"><?php echo ($vo["keyword"]); ?></a><?php endforeach; endif; else: echo "" ;endif; ?></span>
                <div class="fr">
                    <i class="fa fa-globe"></i> Language:
                    <select class="language" autocomplete="off" onchange="changeLanguage(this.value)">
                        <?php if(($lang) == "en-US"): ?><option value="en-US" selected="true">English</option><?php else: ?><option value="en-US">English</option><?php endif; ?>
                        <?php if(($lang) == "zh-TW"): ?><option value="zh-TW" selected="true">繁體中文</option><?php else: ?><option value="zh-TW">繁體中文</option><?php endif; ?>
                        <?php if(($lang) == "zh-CN"): ?><option value="zh-CN" selected="true">简体中文</option><?php else: ?><option value="zh-CN">简体中文</option><?php endif; ?>
                        <?php if(($lang) == "ja-JP"): ?><option value="ja-JP" selected="true">日本語</option><?php else: ?><option value="ja-JP">日本語</option><?php endif; ?>
                        <?php if(($lang) == "ko-KR"): ?><option value="ko-KR" selected="true">한국어</option><?php else: ?><option value="ko-KR">한국어</option><?php endif; ?>
                    </select>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container-fluid view">
    <div class="row">
        <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1"></div>
        <div class="col-xs-11 col-sm-11 col-md-11 col-lg-11 tags">
            <?php if(is_array($tags)): $i = 0; $__LIST__ = $tags;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><a href="/list/<?php echo (urlencode($vo["tag"])); ?>" title="<?php echo ($vo["tag"]); ?>" target="_blank" class="btn btn-info"><?php echo ($vo["tag"]); ?></a><?php endforeach; endif; else: echo "" ;endif; ?>
        </div>
    </div>
</div>
<footer class="footer">
    <p>Copyright &copy;2015 <a href="/"><?php echo (L("site_name")); ?></a>. All Rights Reserved.</p>
</footer>
<div id="gotop"><i class="fa fa-arrow-circle-up"></i></div>
<script src="//cdn.bootcss.com/jquery/2.2.0/jquery.min.js"></script>
<script src="//cdn.bootcss.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
<script src="/static/js/common.js"></script>
</body>
</html>